package com.amazon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login {

	WebDriver driver;
	
	 By userName=By.xpath("//input[@id='ap_email']");
	 By continueButton=By.id("continue");
	 By password=By.id("ap_password");
	 By loginButton=By.id("signInSubmit");
	
	public Login(WebDriver driver){
		this .driver=driver;
	}
	
	public void loginToAmazon(String userId, String pwd){
		driver.findElement(userName).sendKeys(userId);
		driver.findElement(continueButton).click();
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginButton).click();
	}
}
