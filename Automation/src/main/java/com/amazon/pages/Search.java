package com.amazon.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Search {

	WebDriver driver;
	
	 By searchBox = By.id("twotabsearchtextbox");
	 By searchButton = By.xpath("//input[@class='nav-input']");
	 By bookList = By.xpath("//img[@class='s-image']");
	
	public Search(WebDriver driver){
		this.driver=driver;
	}
	
	public  void searchInAmazon(String book){
		driver.findElement(searchBox).sendKeys(book);
		driver.findElement(searchButton).click();
		List<WebElement> listJavaBook = driver.findElements(bookList);
		
		String book1=listJavaBook.get(0).getAttribute("src");
		System.out.println("First java book imge URL: "+book1);
		
		String book2=listJavaBook.get(1).getAttribute("src");
		System.out.println("Second java book imge URL: "+book2);
		
		String book3=listJavaBook.get(2).getAttribute("src");
		System.out.println("Third java book imge URL: "+book3);
	}
}
