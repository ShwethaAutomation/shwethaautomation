package com.amazon.testcases;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.amazon.pages.Login;
import com.amazon.pages.Search;

public class AmazonOrders {

	@Test
	public void orderBook() throws InterruptedException{
	System.setProperty("webdriver.gecko.driver", "C:\\Users\\vshwetha\\Downloads\\geckodriver-v0.24.0-win64\\geckodriver.exe");
	WebDriver driver = new FirefoxDriver();
	driver.get("https://www.amazon.in/");
	
	//implicit wait
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	Search searchpage = new Search(driver);
	//calling Search class(POM)
	searchpage.searchInAmazon("Java book");
	driver.findElement(By.xpath("//span[@class='a-size-medium a-color-base a-text-normal']")).click();

	//handling multiple windows
	String MainWindow=driver.getWindowHandle();		
    // To handle all new opened window.				
    Set<String> s1=driver.getWindowHandles();		
    Iterator<String> i1=s1.iterator();		
    	
    while(i1.hasNext())			
    {		
        String ChildWindow=i1.next();		
    	
        if(!MainWindow.equalsIgnoreCase(ChildWindow))			
        {    		
                // Switching to Child window
                driver.switchTo().window(ChildWindow);
               //explicit wait
                WebDriverWait wait=new WebDriverWait(driver,3);
                WebElement addtocartButton;
                addtocartButton=wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='add-to-cart-button']"))); 
                addtocartButton.click();
                
                driver.findElement(By.xpath("//a[@id='hlb-ptc-btn-native']")).click();
                
                Login loginpage=new Login(driver);
                //calling Login class(POM)
                loginpage.loginToAmazon("7259846584","Shwe1995$");
                
                driver.findElement(By.xpath("//a[contains(text(),'Deliver to this address')]")).click();
                driver.findElement(By.xpath("//div[contains(@class,'a-row')]//div[1]//div[1]//span[1]//span[1]//input[1]")).click();
                Thread.sleep(3000);
        }
	 }
    	driver.quit();
	}	
}
