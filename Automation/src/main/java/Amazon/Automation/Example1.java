package Amazon.Automation;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Example1 {
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\vshwetha\\Downloads\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.amazon.in/");
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Java book");
		driver.findElement(By.xpath("//input[@class='nav-input']")).click();
		
		List<WebElement> listJavaBook = driver.findElements(By.xpath("//img[@class='s-image']"));
		
		String book1=listJavaBook.get(0).getAttribute("src");
		System.out.println("First java book imge URL: "+book1);
		
		String book2=listJavaBook.get(1).getAttribute("src");
		System.out.println("Second java book imge URL: "+book2);
		
		String book3=listJavaBook.get(2).getAttribute("src");
		System.out.println("Third java book imge URL: "+book3);
			
		//scroll the page
//		JavascriptExecutor jse = (JavascriptExecutor)driver;
//		jse.executeScript("window.scrollBy(0,500)", "");
		
		driver.findElement(By.xpath("//span[@class='a-size-medium a-color-base a-text-normal']")).click();
		
		String MainWindow=driver.getWindowHandle();		
		
        // To handle all new opened window.				
            Set<String> s1=driver.getWindowHandles();		
        Iterator<String> i1=s1.iterator();		
        		
        while(i1.hasNext())			
        {		
            String ChildWindow=i1.next();		
            		
            if(!MainWindow.equalsIgnoreCase(ChildWindow))			
            {    		
                 
                    // Switching to Child window
                    driver.switchTo().window(ChildWindow);
                   Thread.sleep(3000);
                    driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click();
                   
                    driver.findElement(By.xpath("//a[@id='hlb-ptc-btn-native']")).click();
                    driver.findElement(By.xpath("//input[@id='ap_email']")).sendKeys("7259846584");
                    driver.findElement(By.id("continue")).click();
                    driver.findElement(By.id("ap_password")).sendKeys("Shwe1995$");
                    driver.findElement(By.id("signInSubmit")).click();
                    driver.findElement(By.xpath("//a[contains(text(),'Deliver to this address')]")).click();
                    driver.findElement(By.xpath("//div[contains(@class,'a-row')]//div[1]//div[1]//span[1]//span[1]//input[1]")).click();
                    Thread.sleep(3000);
                    driver.close();         
            }
        }
	}
}
